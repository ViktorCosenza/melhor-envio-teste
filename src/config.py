import os
import dataclasses
from dataclasses import dataclass
from abc import ABC
import json


@dataclass(frozen=True)
class Config(ABC):
    @classmethod
    def from_env(cls) -> 'Config': 
        kwargs = {field.name: field.type(os.environ[field.name.upper()]) for field in dataclasses.fields(cls)}
        return cls(**kwargs)

    @classmethod
    def from_json(cls, filename: str) -> 'Config':
        with open(filename, 'r') as f: content = json.load(f)
        kwargs = {field.name: field.type(content[field.name]) for field in dataclasses.fields(cls)}
        return cls(**kwargs)

@dataclass(frozen=True)
class ETLConfig(Config):
    output_dir       : str
    seeds_root_dir   : str
    raw_data_filename: str
    pg_host          : str
    pg_user          : str
    pg_password      : str
    pg_database      : str
    pg_schema        : str
