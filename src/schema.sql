CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Dtypes
create domain protocol_dtype as varchar(10);
create domain method_dtype as varchar(10);
create domain status_dtype as varchar(30);
create domain content_dtype as varchar(100);
create domain host_dtype as varchar(32);
create domain url_dtype as varchar(256);
create domain uri_dtype as varchar(256);
create domain header_key_dtype as varchar(256);
create domain header_value_dtype as varchar(256);

-- Enums/Constant tables
create table http_protocols (
    protocol protocol_dtype primary key
);

create table http_status_codes (
    status_code integer primary key,
    status_description varchar(50) not null,
    status_type status_dtype not null
);

create table http_methods (
    method method_dtype primary key
);

create table http_content_types (
    content_type content_dtype primary key
);

-- Services/Routes
create table http_services (
    id uuid primary key default uuid_generate_v4(),
    connect_timeout integer not null,
    host host_dtype not null,
    sname varchar(32) not null,
    spath varchar(32) not null,
    port integer not null,
    protocol protocol_dtype references http_protocols(protocol) not null,
    read_timeout integer not null,
    write_timeout integer not null,
    retries integer not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

create table http_routes (
    id uuid primary key default uuid_generate_v4(),
    service_id uuid references http_services(id),
    hosts host_dtype not null,
    preserve_host boolean not null,
    strip_path boolean not null,
    regex_priority smallint not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

create table http_route_methods (
    method method_dtype references http_methods(method),
    http_route_id uuid references http_routes(id),
    primary key (method, http_route_id)
);

create table http_route_protocols (
    http_route_id uuid references http_routes(id),
    protocol protocol_dtype references http_protocols(protocol),
    primary key(http_route_id, protocol)
);

-- Logs
create table http_logs (
    id uuid primary key default uuid_generate_v4(),
    http_route_id uuid references http_routes(id),
    upstream_uri uri_dtype not null,
    client_ip inet not null,
    started_at timestamp not null,
    auth_entity_consumer_id uuid not null
);

create table http_requests (
    id uuid primary key default uuid_generate_v4(),
    http_log_id uuid unique references http_logs(id),
    method method_dtype references http_methods(method),
    request_url url_dtype not null,
    request_uri url_dtype not null,
    size integer not null
);

create table http_responses (
    id uuid primary key default uuid_generate_v4(),
    http_log_id uuid unique references http_logs(id),
    status_code integer references http_status_codes(status_code) not null,
    size integer not null
);

create table http_request_headers (
    request_id uuid references http_requests(id),
    header_key header_key_dtype not null,
    header_value header_value_dtype not null,
    primary key (request_id, header_key)
);

create table http_response_headers (
    response_id uuid references http_responses(id),
    header_key header_key_dtype not null,
    header_value header_value_dtype not null,
    primary key (response_id, header_key)
);

create table http_latencies (
    id uuid default uuid_generate_v4(),
    http_log_id uuid unique references http_logs(id),
    proxy integer not null,
    kong integer not null,
    request integer not null
);

-- Unused in code but decided to leave for schema consistency with Json data
-- Every entry in the Json is empty for this field
create table http_request_querystrings (
    querystring varchar(128),
    http_log_id uuid references http_logs(id),
    primary key(querystring, http_log_id)
);
