from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple

import psycopg2
import pandas as pd


DBConnection = psycopg2.extensions.connection
DBCursor = psycopg2.extensions.cursor

ExtractRow = Dict[str, Any]
ExtractOutput = List[ExtractRow]

TransformOutput = List[Tuple[str, pd.DataFrame]]


RawColName = str
SchemaColName = str
TableName = str
ColumnTranslation = Dict[RawColName, SchemaColName]

@dataclass(frozen=True)
class TranslationSpec:
    keep: List[str] = field(default_factory=list)
    date_cols: List[str] = field(default_factory=list)
    translate: ColumnTranslation = field(default_factory=dict)

SchemaTranslationSpec = Dict[TableName, TranslationSpec]
