import json
from dtypes import ExtractOutput


def extract(filename: str) -> ExtractOutput:
    with open(filename, 'r') as f: data = f.readlines() 

    data = [json.loads(d) for d in data]
    return data