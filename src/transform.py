import logging
from multiprocessing import Pool
from typing import Any, List

import numpy as np
import pandas as pd
from dtypes import ExtractOutput, ExtractRow, SchemaTranslationSpec, TransformOutput, TranslationSpec
from db import new_uuid

KEY_TRANSLATIONS: SchemaTranslationSpec = {
    'http_services': TranslationSpec(
        keep=['id', 'connect_timeout', 'host', 'port', 'protocol', 'read_timeout', 'write_timeout', 'retries'],
        date_cols=['created_at', 'updated_at'],
        translate={'name': 'sname', 'path': 'spath'},
    ),
    'http_routes': TranslationSpec(
        keep=['id', 'hosts', 'preserve_host', 'strip_path', 'regex_priority', 'service_id'],
        date_cols=['created_at', 'updated_at'],
    ),
    'http_route_methods': TranslationSpec(
        keep=['method', 'http_route_id']
    ),
    'http_route_protocols': TranslationSpec(
        keep=['http_route_id', 'protocol']
    ),
    'http_logs': TranslationSpec(
        keep=['id','http_route_id', 'upstream_uri', 'client_ip', 'auth_entity_consumer_id'],
        date_cols=['started_at']
    ),
    'http_requests': TranslationSpec(
        keep=['id', 'method', 'http_log_id', 'size'],
        translate={'url': 'request_url', 'uri': 'request_uri'},
    ),
    'http_request_headers': TranslationSpec(
        keep=['request_id', 'header_key', 'header_value']
    ),
    'http_responses': TranslationSpec(
        keep=['id', 'http_log_id', 'size'],
        translate={'status': 'status_code'},
    ),
    'http_response_headers': TranslationSpec(
        keep=['response_id', 'header_key', 'header_value']
    ),
    'http_latencies': TranslationSpec(
        keep=['id', 'http_log_id', 'proxy', 'kong', 'request']
    ),
}

def row_add_id(row: ExtractRow) -> ExtractRow:
    log_id = new_uuid() 
    request_id = new_uuid()
    response_id = new_uuid()
    latency_id = new_uuid()

    row['id'] = log_id
    row['http_route_id'] = row['route']['id']

    ## Route
    row['route']['service_id'] = row['service']['id']

    ## Request
    row['request']['id'] = request_id
    row['request']['http_log_id'] = log_id

    # Response
    row['response']['id'] = response_id
    row['response']['http_log_id'] = log_id

    # Latencies
    row['latencies']['id'] = latency_id
    row['latencies']['http_log_id'] = log_id

    return row

def add_ids(data: ExtractOutput) -> ExtractOutput:
    return [row_add_id(row) for row in data]

def to_sql_schema(
    df: pd.DataFrame, 
    spec: TranslationSpec
) -> pd.DataFrame:
    table = df.rename(columns=spec.translate)
    table = table[[*spec.keep, *spec.translate.values(), *spec.date_cols]]

    for col in spec.date_cols: table[col] = pd.to_datetime(table[col], unit='s')

    return table

def series_expand_list(
    s: pd.Series, 
    pkey: str, 
    fkey: str, 
    col_name: str, 
    value_name: str
) -> pd.DataFrame:
    return pd.DataFrame(
        data=[{fkey: s[pkey], col_name: value} for value in s[value_name]]
    )

def df_expand_list(
    df: pd.DataFrame, 
    pkey: str, 
    fkey: str, 
    col_name: str, 
    value_name: str
) -> pd.DataFrame:
    return pd.concat([series_expand_list(row, pkey, fkey, col_name, value_name) for _i, row in df.iterrows()])

def series_expand_dict(
    s: pd.Series, 
    col_name: str,  
    pkey: Any, 
    remove_cols: List=[]
) -> np.ndarray:
    data = np.stack([[pkey, k, v] for k, v  in s[col_name].items() if k not in remove_cols])
    return data
    
def df_expand_dict(
    df: pd.DataFrame, 
    col_name: str, 
    pkey: str, 
    fkey: str, 
    key_name: str, 
    value_name: str,
) -> pd.DataFrame:
    kwargs_list = [(row, col_name, row[pkey], [fkey]) for _i, row in  df.iterrows()]
    data = [series_expand_dict(*kwargs) for kwargs in kwargs_list]

    data = np.concatenate(data)
    return pd.DataFrame(data, columns=[fkey, key_name, value_name])

def to_dataframes(
    data: ExtractOutput, 
    spec: SchemaTranslationSpec,
) -> TransformOutput:
    df = pd.DataFrame(data)

    logging.info('Normalizing Json data')
    logging.info('Normalizing Services')
    services = pd.DataFrame(df['service'].tolist()).drop_duplicates(subset=['id'])

    logging.info('Normalizing Routes')
    routes = pd.DataFrame(df['route'].tolist()).drop_duplicates(subset=['id'])

    logging.info('Normalizing Requests')
    requests = pd.DataFrame(df['request'].tolist())

    logging.info('Normalizing Responses')
    responses = pd.DataFrame(df['response'].tolist())

    logging.info('Normalizing logs')
    df['auth_entity_consumer_id'] = df['authenticated_entity'].apply(lambda row: row['consumer_id']['uuid'])
    logs = df

    logging.info('Normalizing Latencies')
    latencies = pd.DataFrame(df['latencies'].tolist())

    logging.info('Creating tables')
    tables =  [
        ('http_services', services),
        ('http_routes', routes),
        ('http_route_methods', df_expand_list(routes, 'id', 'http_route_id', 'method', 'methods')),
        ('http_route_protocols', df_expand_list(routes, 'id', 'http_route_id', 'protocol', 'protocols')),
        ('http_logs', logs),
        ('http_requests', requests),
        ('http_request_headers', df_expand_dict(
            requests, 
            'headers',
            'id', 
            'request_id', 
            'header_key', 
            'header_value')),
        ('http_responses', responses),
        ('http_response_headers', df_expand_dict(
            responses, 
            'headers',
            'id', 
            'response_id', 
            'header_key', 
            'header_value')),
        ('http_latencies', latencies)
    ]

    logging.info('Translating names')
    tables = [
        (
            name, 
            to_sql_schema(table, spec[name])
        ) for name, table in tables
    ]

    return tables

def transform(
    data: ExtractOutput, 
    column_translations_dict: SchemaTranslationSpec,
) -> TransformOutput: 
    logging.info('Creating UUIDs')
    data = add_ids(data)

    logging.info('Converting do Dataframe Tables')
    tables = to_dataframes(data, column_translations_dict)
    return tables