from argparse import ArgumentParser
import logging

from config import ETLConfig

from extract import extract
from db import create_connection, create_schema, drop_schema, seed_enums
from load import load
from report import report, QUERIES as REPORT_QUERIES
from transform import KEY_TRANSLATIONS, transform


def parse_args(): 
    ag = ArgumentParser()
    ag.add_argument('--seed', action='store_true', default=False, help='Seed tables that are always the same (HTTP Protocols and Status Codes)')
    ag.add_argument('--config', required=True, choices=['env', 'json'], default='env', help='Config variables source')
    ag.add_argument('--config-filename', required=False, default='config.json', help='Config filename, only applicable if config type requires a file')

    ag.add_argument('--create-schema', action='store_true', help='Create SQL Schema')
    ag.add_argument('--reset-db', action='store_true', help='Drop database before starting job')
    ag.add_argument('--load', action='store_true', help='Perform load to DB operation')
    ag.add_argument('--verboose', action='store_true', help='Log information to stdout')

    ag.add_argument('--report-stdout', action='store_true', help='Output Report to stdout')
    ag.add_argument('--report-csv', action='store_true', help='Output Report as csv')
    return ag.parse_args()


def main(): 
    args = parse_args()

    if args.verboose: logging.basicConfig(level=logging.INFO)

    if args.config == 'json':
        config = ETLConfig.from_json(args.config_filename)
    elif args.config == 'env':
        config = ETLConfig.from_env()

    config: ETLConfig

    conn = create_connection(config)

    if args.reset_db:
        logging.info('Reseting Schema')
        drop_schema(conn, schema=config.pg_schema)

    if args.create_schema or args.reset_db:
        logging.info('Creating Schema')
        create_schema(conn)

    if args.seed or args.reset_db: 
        logging.info('Seeding DB')
        seed_enums(conn, config.seeds_root_dir)

    if args.load:
        logging.info('Extracting data')
        data = extract(config.raw_data_filename)
        logging.info('Transforming data')
        data = transform(data, KEY_TRANSLATIONS)
        logging.info('Loading data')
        load(conn, data)

    if args.report_stdout or args.report_csv: 
        logging.info('Generating report')
        report(
            conn, 
            output_dir=config.output_dir if args.report_csv else None, 
            output_std_out=args.report_stdout, 
            queries=REPORT_QUERIES
        )


if __name__ == '__main__': main()