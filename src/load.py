import logging
from db import sql_bulk_insert
from dtypes import DBConnection, TransformOutput


def load(conn: DBConnection, dfs: TransformOutput, nthreads: int=1):
    logging.info('Executing Bulk inserts')
    with conn:
        with conn.cursor() as cursor:
            for (name, df) in dfs: 
                logging.info(f'SQL generate insert: {name}')
                sql = sql_bulk_insert(cursor, name, df)
                logging.info(f'SQL execute insert: {name}')
                cursor.execute(sql)