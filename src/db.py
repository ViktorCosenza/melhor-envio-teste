from os import path
import os
import uuid
import numpy as np
import pandas as pd

import psycopg2

from config import ETLConfig
from dtypes import DBConnection, DBCursor



def new_uuid(): return str(uuid.uuid4())
def create_connection(config: ETLConfig) -> DBConnection:
    return psycopg2.connect(
        host=config.pg_host,
        dbname=config.pg_database,
        user=config.pg_user,
        password=config.pg_password,
    )

def sql_bulk_insert(cursor: DBCursor, table: str, df: pd.DataFrame) -> str:
    colnames = ','.join(df.columns)

    colvalues = ('%s,' * len(df.columns))[:-1]
    colvalues = f'({colvalues}),' * len(df)
    colvalues = colvalues[:-1]
    colvalues = cursor.mogrify(colvalues, np.array(df.values.flat)).decode()

    return f'''
        insert into {table} 
            ({colnames}) values 
            {colvalues};
    '''

def seed_enums(conn: DBConnection, root_dir: str):
    with conn:
        with conn.cursor() as cursor:
            for filename in os.listdir(root_dir):
                sql = sql_bulk_insert(
                    cursor=cursor,
                    table=filename.replace('.csv', ''), 
                    df=pd.read_csv(path.join(root_dir, filename))
                )

                cursor.execute(sql)

def create_schema(conn: DBConnection):
    with open('schema.sql') as f:
        content = f.read()
        with conn:
            with conn.cursor() as cursor:
                cursor: DBCursor
                cursor.execute(content)

def drop_schema(conn: DBConnection, schema: str='public'):
    with conn.cursor() as cursor:
        cursor.execute(f'drop schema {schema} cascade;')
        cursor.execute(f'create schema {schema};')