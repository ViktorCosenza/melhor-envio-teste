import logging
from os import path
from typing import List, Optional, Tuple
import pandas as pd

from dtypes import DBConnection

REQUESTS_PER_SERVICE = (
    'Requests_Per_Service', 
    '''
    select 
        http_routes.service_id, 
        count(*) requests 
    from http_requests hr 
    join http_logs hl on hl.id = hr.http_log_id 
    join http_routes ON http_routes.id = hl.http_route_id 
    group by http_routes.service_id;''')

REQUESTS_PER_CONSUMER = (
    'Requests_Per_Consumer',
    'select hl.auth_entity_consumer_id, count(*) from http_logs hl group by hl.auth_entity_consumer_id order by count(*) desc;'
)

AVG_LATENCIES = (
    'Avg_Latencies', 
    '''
    select 
        hr.service_id, 
        avg(hla.kong) kong_avg, 
        avg(hla.proxy) proxy_avg, 
        avg(hla.request) request_avg 
    from http_logs hl 
    join http_latencies hla on hla.http_log_id = hl.id 
    join http_routes hr on hr.id = hl.http_route_id 
    group by hr.service_id;'''
)

QUERIES = [REQUESTS_PER_SERVICE, REQUESTS_PER_CONSUMER, AVG_LATENCIES]

def report(
    conn: DBConnection, 
    queries: List[Tuple[str, str]], 
    output_dir: Optional[str]='.',
    output_std_out: bool=True
):
    if not (output_std_out or (output_dir is not None)): return

    for name, sql in queries:
        df: pd.DataFrame = pd.read_sql_query(con=conn, sql=sql)

        if output_dir is not None:
            filename = path.join(output_dir, f'{name}.csv')
            logging.info(f'Saving csv to {filename}')
            df.to_csv(filename, index=False)

        if output_std_out: 
            print(df.to_string(index=False))

            