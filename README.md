
# Melhor Envio - Data Engineer
## Requisitos
- docker
- docker-compose

## Instruções
### Buildar imagem do app
Dentro da pasta src:
```
cd src
docker build . --tag python-melhor-envio-etl
```

### Rodar a aplicação pela primeira vez com logs de processo
#### Arquivo fonte
1. Coloque o arquivo fonte logs.txt em data/logs/logs.txt

#### Na pasta root do repositório:
Certifique-se que não há nada rodando na porta 5432 local. 
Caso não seja possível liberar a porta remova a linha -port 5432:5432 em docker-compose antes de rodar o compando a seguir.

2. Iniciar um bash dentro do container
```
docker-compose run etl bash
```

3. Rodar o processo
```
python main.py --config env --create-schema --seed --load --report-csv --verboose
```

- O processo completo leva entre 1 e 2 minutos em um i72600k e consome 1-2GB de RAM.
- Saída: 3 arquivos CSV com o resultado das consultas.

Os csv são gerados em data/output/<nome>.csv. 
Os arquivos gerados estarão acessíveis tanto na imagem docker quanto na máquina local, pois a pasta ./data é montada como volume dentro da imagem docker.

#### Rodar apenas o report final com logs de processo
```
docker-compose run etl bash
python main.py --config env --report-csv --verboose
```

#### Refazer o processo (Limpar o banco, recriar o schema, recarregar os dados)
```
docker-compose run etl bash
python main.py --config env --reset-db --load --report-csv
```
- Usar Ctrl-C para sair do bash

#### Desligar a aplicação
```
docker-compose down
```

#### Conectando ao banco a partir de um cliente local 
O container mapeia a porta interna 5432 do container para a máquina local.
Basta conectar normalmente via Postgres na porta 5432 local.

host: localhost

db: postgres

user: postgres

password: secret

schema: public

## Parâmetros linha de comando
- seed Popula as tabelas de métodos HTTP e 
- config {env,json} Origem da configuração. Json ou variaveis de ambiente (json para dev, env para rodar)
- config-filename Arquivo json de configuração, não aplicável para --config=env
- create-schema Criar tabelas no banco. Usar apenas na primeira vez depois de inicializar a aplicação
- reset-db Deletar o schema, recriar e fazer o seed
- load Carregar 
- verboose logar informações para o terminal sobre o status da aplicação
- report-stdout Printar o resultado na saída
- report-csv Salvar CSVs 

## Variaveis de ambiente/config.json
- OUTPUT_DIR pasta onde salvar os resultados em csv
- SEEDS_ROOT_DIR pasta que contém os seeds. cada seed é um arquivo csv com as mesmas colunas presentes no banco na tabela de mesmo nome
- RAW_DATA_FILENAME path para o arquivo logs.txt
- PG_HOST Host postgres
- PG_USER User postgres
- PG_PASSWORD Senha postgres
- PG_DATABASE Database postgres
- PG_SCHEMA Schema


#### Ajuda
```
python main.py --help
usage: main.py [-h] [--seed] --config {env,json} [--config-filename CONFIG_FILENAME] [--create-schema]
               [--reset-db] [--load] [--verboose] [--report-stdout] [--report-csv]

optional arguments:
  -h, --help            show this help message and exit
  --seed                Seed tables that are always the same (HTTP Protocols and Status Codes)
  --config {env,json}   Config variables source
  --config-filename CONFIG_FILENAME
                        Config filename, only applicable if config type requires a file
  --create-schema       Create SQL Schema
  --reset-db            Drop database before starting job
  --load                Perform load to DB operation
  --verboose            Log information to stdout
  --report-stdout       Output Report to stdout
  --report-csv          Output Report as csv
```


# Teste - Instruções
### Tecnologias

- Python
- MySQL

--------------

### Instruções

O arquivo [logs.txt](https://drive.google.com/open?id=1GliYD4Q19_f6S88iFsn0dk8dGLhB9YXF) contém informações de log geradas por um sistema gateway, cada solicitação foi registrada em um objeto JSON separado por uma nova linha `\n`, com o seguinte formato:

```

{
    "request": {
        "method": "GET",
        "uri": "/get",
        "url": "http://httpbin.org:8000/get",
        "size": "75",
        "querystring": {},
        "headers": {
            "accept": "*/*",
            "host": "httpbin.org",
            "user-agent": "curl/7.37.1"
        },
    },
    "upstream_uri": "/",
    "response": {
        "status": 200,
        "size": "434",
        "headers": {
            "Content-Length": "197",
            "via": "kong/0.3.0",
            "Connection": "close",
            "access-control-allow-credentials": "true",
            "Content-Type": "application/json",
            "server": "nginx",
            "access-control-allow-origin": "*"
        }
    },
    "authenticated_entity": {
        "consumer_id": "80f74eef-31b8-45d5-c525-ae532297ea8e"
    },
    "route": {
        "created_at": 1521555129,
        "hosts": null,
        "id": "75818c5f-202d-4b82-a553-6a46e7c9a19e",
        "methods": ["GET","POST","PUT","DELETE","PATCH","OPTIONS","HEAD"],
        "paths": [
            "/example-path"
        ],
        "preserve_host": false,
        "protocols": [
            "http",
            "https"
        ],
        "regex_priority": 0,
        "service": {
            "id": "0590139e-7481-466c-bcdf-929adcaaf804"
        },
        "strip_path": true,
        "updated_at": 1521555129
    },
    "service": {
        "connect_timeout": 60000,
        "created_at": 1521554518,
        "host": "example.com",
        "id": "0590139e-7481-466c-bcdf-929adcaaf804",
        "name": "myservice",
        "path": "/",
        "port": 80,
        "protocol": "http",
        "read_timeout": 60000,
        "retries": 5,
        "updated_at": 1521554518,
        "write_timeout": 60000
    },
    "latencies": {
        "proxy": 1430,
        "kong": 9,
        "request": 1921
    },
    "client_ip": "127.0.0.1",
    "started_at": 1433209822425
}

```

***Algumas considerações sobre o objeto JSON acima:***

`latencies` contém alguns dados sobre as latências envolvidas:

-   `proxy`  é o tempo levado pelo serviço final para processar a requisição.
-   `kong`  é a latência referente a execução de todos os plugins pelo Kong (gateway).
-   `request`  é o tempo decorrido entre o primeiro byte ser lido do cliente e o último byte ser enviado a ele. Útil para detectar clientes lentos.

--------------

### Requisitos

- Processar o arquivo de log, extrair informações e salvá-las em um banco de dados.
- Estruturar esse banco de forma relacional.
- Buscar a normalização desse banco de dados.
- Gerar um relatório para cada descrição abaixo, em formato csv:
	- Requisições por consumidor;
	- Requisições por serviço;
	- Tempo médio de request, proxy e kong por serviço.
- Documentar passo a passo de como executar o teste através de um arquivo `README.md`.
- Efetue o `commit` de todos os passos do desenvolvimento em um ***git público*** de sua preferência e disponibilize apenas o link para o repositório.

